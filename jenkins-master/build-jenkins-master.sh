latest_lts_version=2.138.3
sha=953e4dda2d3065284c0016b3e8279e097f830c128b1f712d84780ff2b0751e7d

docker build -t jenkins-master:$latest_lts_version \
    --build-arg "JENKINS_VERSION=$latest_lts_version" \
    --build-arg "JENKINS_SHA=$sha" \
    --file jenkins-master.dockerfile .
