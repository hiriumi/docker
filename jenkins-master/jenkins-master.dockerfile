FROM centos:latest
RUN yum install -y epel-release
RUN yum update -y
RUN yum install -y git curl ca-certificates java-1.8.0-openjdk-devel.x86_64

ARG user=amaterasu48
ARG group=amaterasu48
ARG uid=1000
ARG gid=1000
ARG http_port=8080
ARG agent_port=50000
ARG JENKINS_HOME=/var/jenkins_home
ARG CERT_DIR=/etc/pki/ca-trust/source/anchors

ENV JENKINS_HOME $JENKINS_HOME
ENV JENKINS_SLAVE_AGENT_PORT ${agent_port}

RUN mkdir -p $JENKINS_HOME && chown ${uid}:${gid} $JENKINS_HOME \
    && groupadd -g ${gid} ${group} && useradd -d "$JENKINS_HOME" -u ${uid} -g ${gid} -m -s /bin/bash ${user}

VOLUME $JENKINS_HOME

#RUN mkdir -p /usr/share/jenkins/ref/init.groovy.d

ARG TINI_VERSION=v0.18.0
ARG ARCHITECTURE=amd64

COPY tini_pub.gpg ${JENKINS_HOME}/tini_pub.gpg
RUN curl -fsSL https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini-static-${ARCHITECTURE} -o /sbin/tini \
    && curl -fsSL https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini-static-${ARCHITECTURE}.asc -o /sbin/tini.asc \
    && gpg --import ${JENKINS_HOME}/tini_pub.gpg \
    && gpg --verify /sbin/tini.asc \
    && rm -rf /sbin/tini.asc /root/.gnupg \
    && chmod +x /sbin/tini

COPY init.groovy /usr/share/jenkins/ref/init.groovy.d/tcp-slave-agent-port.groovy

# Copy SSL certificate and register
# COPY ./certs/Pacific-CA.cer $CERT_DIR
# COPY ./certs/poc-ssl.crt $CERT_DIR
# COPY ./certs/us01p01bld05.cer $CERT_DIR
# RUN update-ca-trust

ARG JENKINS_VERSION=2.138.2
ENV JENKINS_VERSION ${JENKINS_VERSION:-2.121.1}

# jenkins.war checksum, download will be validated using it.
# Checksum can be found here. https://updates.jenkins-ci.org/download/war/
ARG JENKINS_SHA
ARG JENKINS_URL=https://repo.jenkins-ci.org/public/org/jenkins-ci/main/jenkins-war/${JENKINS_VERSION}/jenkins-war-${JENKINS_VERSION}.war
# Download the jenkins.war file from the official site
RUN curl -fsSL ${JENKINS_URL} -o /usr/share/jenkins/jenkins.war && echo "${JENKINS_SHA} /usr/share/jenkins/jenkins.war" | sha256sum -c -

ENV JENKINS_UC https://updates.jenkins.io
ENV JENKINS_UC_EXPERIMENTAL=https://updates.jenkins.io/experimental
ENV JENKINS_INCREMENTALS_REPO_MIRROR=https://repo.jenkins-ci.org/incrementals

RUN chown -R ${user} "${JENKINS_HOME}" /usr/share/jenkins/ref
COPY jenkins-support /usr/local/bin/jenkins-support
COPY jenkins.sh /usr/local/bin/jenkins.sh
COPY tini-shim.sh /bin/tini
RUN chown ${user} "/usr/local/bin/jenkins.sh"
RUN chown ${user} "/bin/tini"
RUN chmod 755 "/usr/local/bin/jenkins.sh"
RUN chmod 755 "/bin/tini"

EXPOSE ${http_port}
EXPOSE ${agent_port}

ENV COPY_REFERENCE_FILE_LOG $JENKINS_HOME/copy_reference_file.log

USER ${user}

ENTRYPOINT ["/sbin/tini", "--", "/usr/local/bin/jenkins.sh"]

# from a derived Dockerfile, can use `RUN plugins.sh active.txt` to setup /usr/share/jenkins/ref/plugins from a support bundle
COPY plugins.sh /usr/local/bin/plugins.sh
COPY install-plugins.sh /usr/local/bin/install-plugins.sh
