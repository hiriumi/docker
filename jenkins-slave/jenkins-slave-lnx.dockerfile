FROM centos:latest
ARG REMOTING_VERSION=3.9
ARG GROOVY_VERSION=2.5.3
ARG USER=unknown
ARG USERID=unknown
ARG GROUP=unknown
ARG GROUPID=10000
ENV HOME /home/jenkins
ARG AGENT_WORKDIR=$HOME/agent
#CentOS base image has the following directory.
ARG CERT_DIR=/etc/pki/ca-trust/source/anchors 

LABEL Description="This is a base image for Jenkins slave communicating with master via JNLP protocol."
# Do some validations
RUN if [ $REMOTING_VERSION = 'unknown' ]; then echo 'Please pass REMOTING_VERSION parameter from command line --build-arg'; exit 1; fi
RUN if [ $USER = 'unknown' ]; then echo 'Please pass USER parameter from command line --build-arg'; exit 1; fi
RUN if [ $USERID = 'unknown' ]; then echo 'Please pass USERID parameter from command line --build-arg'; exit 1; fi
RUN if [ $GROUP = 'unknown' ]; then echo 'Please pass GROUP parameter from command line --build-arg'; exit 1; fi

# Add local group for Jenkins
RUN groupadd -g $GROUPID $GROUP
RUN useradd -l -u $USERID -g $GROUP -d $HOME $USER

# Create home directory.
RUN mkdir -p /home/jenkins/.jenkins && mkdir -p $AGENT_WORKDIR && mkdir -p /home/jenkins/.ssh

# Copy necessary files
COPY connect.sh /usr/share/jenkins/connect.sh

RUN chown $USER:$GROUP -R /home/jenkins

RUN echo "Installing epel-release..."
RUN yum install -y epel-release

RUN echo "Installing packages..."
RUN yum install -y ca-certificates git lsof java-1.8.0-openjdk-headless python36.x86_64 python36-libs.x86_64 python36-devel.x86_64 python36-setuptools python36-requests which zip unzip wget
RUN yum update -y
RUN yum clean all

# Install Groovy SDK
RUN wget https://dl.bintray.com/groovy/maven/apache-groovy-sdk-$GROOVY_VERSION.zip
RUN unzip apache-groovy-sdk-$GROOVY_VERSION.zip
RUN mkdir /usr/share/groovy
RUN mv groovy-$GROOVY_VERSION /usr/share/groovy && ln -s /usr/share/groovy/groovy-$GROOVY_VERSION /usr/share/groovy/current
RUN rm -rf ~/apache-groovy-sdk-$GROOVY_VERSION.zip

# Trust the root and intermediate certificates
RUN update-ca-trust

RUN curl --create-dirs -sSLo /usr/share/jenkins/slave.jar http://repo.jenkins-ci.org/public/org/jenkins-ci/main/remoting/$REMOTING_VERSION/remoting-$REMOTING_VERSION.jar
RUN chmod 755 /usr/share/jenkins
RUN chmod 644 /usr/share/jenkins/slave.jar
RUN chmod 755 /usr/share/jenkins/connect.sh

USER $USERID

ENV AGENT_WORKDIR=$AGENT_WORKDIR
VOLUME /home/jenkins/.jenkins
VOLUME $AGENT_WORKDIR
WORKDIR $HOME
#CMD ["/home/jenkins/connect.sh"]
CMD ["/usr/share/jenkins/connect.sh"]
