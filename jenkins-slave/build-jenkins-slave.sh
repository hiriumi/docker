REMOTING_VERSION='3.9'
GROOVY_VERSION='2.5.3'
USER='amaterasu48'
USERID='1000'
GROUP='amaterasu48'

docker build -f jenkins-slave-lnx.dockerfile -t jenkins-slave-lnx:$REMOTING_VERSION --build-arg REMOTING_VERSION=$REMOTING_VERSION --build-arg USER=$USER --build-arg USERID=$USERID --build-arg GROUP=$GROUP --build-arg GROOVY_VERSION=$GROOVY_VERSION .

docker tag jenkins-slave-lnx:$REMOTING_VERSION jenkins-slave-lnx:$REMOTING_VERSION
